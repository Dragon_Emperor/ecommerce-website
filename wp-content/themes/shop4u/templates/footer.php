<?php 	
$shop4u_page_footer   	 = ( get_post_meta( get_the_ID(), 'page_footer_style', true ) != '' ) ? get_post_meta( get_the_ID(), 'page_footer_style', true ) : sw_options( 'footer_style' );
$shop4u_copyright_text 	 = sw_options( 'footer_copyright' );
$shop4u_copyright_footer = get_post_meta( get_the_ID(), 'copyright_footer_style', true );
$shop4u_copyright_footer  = ( get_post_meta( get_the_ID(), 'copyright_footer_style', true ) != '' ) ? get_post_meta( get_the_ID(), 'copyright_footer_style', true ) : sw_options('copyright_style');
?>
<footer id="footer" class="footer default theme-clearfix">
	<!-- Content footer -->
	<div class="container">
		<?php 
		if( $shop4u_page_footer != '' ) :
			echo sw_get_the_content_by_id( $shop4u_page_footer );
		endif;
		?>
	</div>
	<div class="footer-copyright <?php echo esc_attr( $shop4u_copyright_footer ); ?>">
		<div class="container">
			<!-- Copyright text -->
			<?php if (is_active_sidebar('footer-copyright')){ ?>
			<div class="sidebar-copyright">
				<?php dynamic_sidebar('footer-copyright'); ?>
			</div>
			<?php } ?>
			<div class="copyright-text">
				<?php if( $shop4u_copyright_text == '' ) : ?>
					<p>&copy;<?php echo date('Y') .' '. esc_html__('WordPress Theme SW Shop4u. All Rights Reserved. Designed by ','shop4u'); ?><a class="mysite" href="#"><?php esc_html_e('WPThemeGo.Com','shop4u');?></a>.</p>
				<?php else : ?>
					<?php echo wp_kses( $shop4u_copyright_text, array( 'a' => array( 'href' => array(), 'title' => array(), 'class' => array() ), 'p' => array()  ) ) ; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</footer>