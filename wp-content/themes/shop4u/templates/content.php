<?php get_template_part('header'); ?>
<?php 
	$shop4u_sidebar_template = sw_options('sidebar_blog') ;
	$shop4u_blog_styles = ( sw_options('blog_layout') ) ? sw_options('blog_layout') : 'list';
?>

<div class="shop4u_breadcrumbs">
	<div class="container">
		<?php
			if (!is_front_page() ) {
				if (function_exists('shop4u_breadcrumb')){
					shop4u_breadcrumb('<div class="breadcrumbs custom-font theme-clearfix">', '</div>');
				} 
			} 
		?>
	</div>
</div>

<div class="container">
	<div class="row sidebar-row">
		<!-- Left Sidebar -->
		<?php if ( is_active_sidebar('left-blog') && $shop4u_sidebar_template == 'left' ):
			$shop4u_left_span_class = 'col-lg-'.sw_options('sidebar_left_expand');
			$shop4u_left_span_class .= ' col-md-'.sw_options('sidebar_left_expand_md');
			$shop4u_left_span_class .= ' col-sm-'.sw_options('sidebar_left_expand_sm');
		?>
		<aside id="left" class="sidebar <?php echo esc_attr($shop4u_left_span_class); ?>">
			<?php dynamic_sidebar('left-blog'); ?>
		</aside>

		<?php endif; ?>
		
		<div class="category-contents <?php shop4u_content_blog(); ?>">
			<div class="listing-title">			
				<h1><span><?php shop4u_title(); ?></span></h1>				
			</div>
			<!-- No Result -->
			<?php if (!have_posts()) : ?>
			<?php get_template_part('templates/no-results'); ?>
			<?php endif; ?>			
			
			<?php 
				$shop4u_blogclass = 'blog-content blog-content-'. $shop4u_blog_styles;
				if( $shop4u_blog_styles == 'grid' ){
					$shop4u_blogclass .= ' row';
				}
			?>
			<div class="<?php echo esc_attr( $shop4u_blogclass ); ?>">
			<?php 			
				while( have_posts() ) : the_post();
					get_template_part( 'templates/content', $shop4u_blog_styles );
				endwhile;
			?>
			<?php get_template_part('templates/pagination'); ?>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- Right Sidebar -->
		<?php if ( is_active_sidebar('right-blog') && $shop4u_sidebar_template =='right' ):
			$shop4u_right_span_class = 'col-lg-'.sw_options('sidebar_right_expand');
			$shop4u_right_span_class .= ' col-md-'.sw_options('sidebar_right_expand_md');
			$shop4u_right_span_class .= ' col-sm-'.sw_options('sidebar_right_expand_sm');
		?>
		<aside id="right" class="sidebar <?php echo esc_attr($shop4u_right_span_class); ?>">
			<?php dynamic_sidebar('right-blog'); ?>
		</aside>
		<?php endif; ?>
	</div>
</div>
<?php get_template_part('footer'); ?>
