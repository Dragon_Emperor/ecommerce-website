<?php 
/* 
** Content Header
*/
$shop4u_colorset  	= sw_options('scheme');
$shop4u_logo 		= sw_options('sitelogo');
$sticky_menu 			= sw_options( 'sticky_menu' );
$shop4u_menu_item 	= ( sw_options( 'menu_number_item' ) ) ? sw_options( 'menu_number_item' ) : 9;
$shop4u_title_text   = sw_options('menu_title_text');
$shop4u_more_text 	= ( sw_options( 'menu_more_text' ) )	 ? sw_options( 'menu_more_text' )		: esc_html__( 'See More', 'shop4u' );
$shop4u_less_text 	= ( sw_options( 'menu_less_text' ) )	 ? sw_options( 'menu_less_text' )		: esc_html__( 'See Less', 'shop4u' );
?>
<header id="header" class="header header-style2">
	<!-- Sidebar Top Menu -->
	<?php if (is_active_sidebar('top2')) {?>	
	<div class="header-top">
		<div class="container">
			<?php dynamic_sidebar('top2'); ?>
		</div>
	</div>
	<?php }?>
	<div class="header-mid">
		<div class="container">
			<div class="row">
				<!-- Logo -->
				<div class="top-header col-lg-3 col-md-3 pull-left">
					<div class="shop4u-logo">
						<?php shop4u_logo(); ?>
					</div>
				</div>
				<!-- Sidebar Top Menu -->
				<?php if (is_active_sidebar('mid-header2')) {?>
				<div  class="mid-header pull-right">
					<?php dynamic_sidebar('mid-header2'); ?>
				</div>
				<?php }?>
				<?php if( !sw_options( 'disable_search' ) ) : ?>
					<div class="search-cate col-lg-4 col-md-4 pull-right">
						<?php if( is_active_sidebar( 'search' ) && class_exists( 'sw_woo_search_widget' ) ): ?>
							<?php dynamic_sidebar( 'search' ); ?>
						<?php else : ?>
							<div class="widget shop4u_top non-margin pull-right">
								<div class="widget-inner">
									<?php get_template_part( 'widgets/sw_top/searchcate' ); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="row">
				<?php if ( has_nav_menu('vertical_menu') ) {?>
				<div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 vertical_megamenu vertical_megamenu-header pull-left">
					<div class="mega-left-title">
						<?php if ($shop4u_title_text == "") { ?>
						<strong><?php esc_html_e('All Departments','shop4u'); ?></strong>
						<?php } else { ?>
						<strong><?php esc_attr( $shop4u_title_text ); ?></strong>
						<?php } ?>
					</div>
					<div class="vc_wp_custommenu wpb_content_element">
						<div class="wrapper_vertical_menu vertical_megamenu" data-number="<?php echo esc_attr( $shop4u_menu_item ); ?>" data-moretext="<?php echo esc_attr( $shop4u_more_text ); ?>" data-lesstext="<?php echo esc_attr( $shop4u_less_text ); ?>">
							<?php wp_nav_menu(array('theme_location' => 'vertical_menu', 'menu_class' => 'nav vertical-megamenu')); ?>
						</div>
					</div>
				</div>
				<?php } ?>
				<!-- Primary navbar -->
				<?php if ( has_nav_menu('primary_menu') ) { ?>
				<div id="main-menu" class="main-menu clearfix pull-left">
					<nav id="primary-menu" class="primary-menu">
						<div class="mid-header clearfix">
							<div class="navbar-inner navbar-inverse">
								<?php
								$shop4u_menu_class = 'nav nav-pills';
								if ( 'mega' == sw_options('menu_type') ){
									$shop4u_menu_class .= ' nav-mega';
								} else $shop4u_menu_class .= ' nav-css';
								?>
								<?php wp_nav_menu(array('theme_location' => 'primary_menu', 'menu_class' => $shop4u_menu_class)); ?>
							</div>
						</div>
					</nav>
				</div>			
				<?php } ?>
				<!-- /Primary navbar -->
				<!-- Sidebar Top Menu -->
				<?php if (is_active_sidebar('bottom-header2')) {?>
				<div  class="bottom-header col-lg-2 col-md-2 pull-right">
					<?php dynamic_sidebar('bottom-header2'); ?>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</header>