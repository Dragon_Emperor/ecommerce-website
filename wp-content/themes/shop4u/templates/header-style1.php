<?php 
/* 
** Content Header
*/
$shop4u_colorset  	= sw_options('scheme');
$shop4u_logo 		= sw_options('sitelogo');
$sticky_menu 			= sw_options( 'sticky_menu' );
$shop4u_menu_item 	= ( sw_options( 'menu_number_item' ) ) ? sw_options( 'menu_number_item' ) : 9;
$shop4u_title_text   = sw_options('menu_title_text');
$shop4u_more_text 	= ( sw_options( 'menu_more_text' ) )	 ? sw_options( 'menu_more_text' )		: esc_html__( 'See More', 'shop4u' );
$shop4u_less_text 	= ( sw_options( 'menu_less_text' ) )	 ? sw_options( 'menu_less_text' )		: esc_html__( 'See Less', 'shop4u' );
?>
<header id="header" class="header header-style1">
	<!-- Sidebar Top Menu -->
	<?php if (is_active_sidebar('top')) {?>	
	<div class="header-top">
		<div class="container">
			<?php dynamic_sidebar('top'); ?>
		</div>
	</div>
	<?php }?>
	<div class="header-mid">
		<div class="container">
			<div class="row">
				<!-- Logo -->
				<div class="top-header col-lg-3 col-md-2 pull-left">
					<div class="shop4u-logo">
						<?php shop4u_logo(); ?>
					</div>
				</div>
				<!-- Sidebar Top Menu -->
				<?php if (is_active_sidebar('mid-header')) {?>
				<div  class="mid-header pull-right">
					<?php dynamic_sidebar('mid-header'); ?>
				</div>
				<?php }?>
				<?php if( !sw_options( 'disable_search' ) ) : ?>
					<div class="search-cate col-lg-6 col-md-6 pull-right">
						<?php if( is_active_sidebar( 'search' ) && class_exists( 'sw_woo_search_widget' ) ): ?>
							<?php dynamic_sidebar( 'search' ); ?>
						<?php else : ?>
							<div class="widget shop4u_top non-margin pull-right">
								<div class="widget-inner">
									<?php get_template_part( 'widgets/sw_top/searchcate' ); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="row">
				<!-- Primary navbar -->
				<?php if ( has_nav_menu('primary_menu') ) { ?>
				<div id="main-menu" class="main-menu clearfix col-lg-9 col-md-9 pull-left">
					<nav id="primary-menu" class="primary-menu">
						<div class="mid-header clearfix">
							<div class="navbar-inner navbar-inverse">
								<?php
								$shop4u_menu_class = 'nav nav-pills';
								if ( 'mega' == sw_options('menu_type') ){
									$shop4u_menu_class .= ' nav-mega';
								} else $shop4u_menu_class .= ' nav-css';
								?>
								<?php wp_nav_menu(array('theme_location' => 'primary_menu', 'menu_class' => $shop4u_menu_class)); ?>
							</div>
						</div>
					</nav>
				</div>			
				<?php } ?>
				<!-- /Primary navbar -->
				<!-- Sidebar Top Menu -->
				<?php if (is_active_sidebar('bottom-header')) {?>
				<div  class="bottom-header col-lg-3 col-md-3 pull-right">
					<?php dynamic_sidebar('bottom-header'); ?>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</header>