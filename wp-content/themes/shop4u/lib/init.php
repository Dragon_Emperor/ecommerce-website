<?php
/**
 * shop4u initial setup and constants
 */
function shop4u_setup() {
	// Make theme available for translation
	load_theme_textdomain( 'shop4u', get_template_directory() . '/lang' );

	// Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
	register_nav_menus(array(
		'primary_menu' => esc_html__('Primary Menu', 'shop4u'),
		'vertical_menu' => esc_html__( 'Vertical Menu', 'shop4u' ),
		'mobile_menu' => esc_html__( 'Mobile Menu', 'shop4u' ),
	));
	
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	if( sw_options( 'product_zoom' ) ) :
		add_theme_support( 'wc-product-gallery-zoom' );
	endif;
	
	add_image_size( 'shop4u_blog-responsive1', 390, 280, true );
	add_image_size( 'shop4u_shop-image', 440, 440, true );
	add_image_size( 'shop4u_detail_thumb', 870, 450, true );


	add_theme_support( "title-tag" );
	
	add_theme_support('bootstrap-gallery');     // Enable Bootstrap's thumbnails component on [gallery]
	
	// Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
	add_theme_support('post-thumbnails');

	// Add post formats (http://codex.wordpress.org/Post_Formats)
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
	
	// Custom image header
	$shop4u_header_arr = array(
		'default-image' => get_template_directory_uri().'/assets/img/logo-default.png',
		'uploads'       => true
	);
	add_theme_support( 'custom-header', $shop4u_header_arr );
	
	// Custom Background 
	$shop4u_bgarr = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);
	add_theme_support( 'custom-background', $shop4u_bgarr );
	
	// Tell the TinyMCE editor to use a custom stylesheet
	add_editor_style( 'css/editor-style.css' );
	
	new Shop4u_Menu();
}
add_action('after_setup_theme', 'shop4u_setup');

