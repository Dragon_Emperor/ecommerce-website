<?php
/***** Active Plugin ********/
require_once( get_template_directory().'/lib/class-tgm-plugin-activation.php' );

add_action( 'tgmpa_register', 'shop4u_register_required_plugins' );
function shop4u_register_required_plugins() {
    $plugins = array(
      array(
        'name'               => esc_html__( 'WooCommerce', 'shop4u' ), 
        'slug'               => 'woocommerce', 
        'required'           => true, 
        'version'			 => '3.5.5'
        ),

      array(
        'name'     			 => esc_html__( 'SW Core', 'shop4u' ),
        'slug'      		 => 'sw_core',
        'source'             => get_template_directory_uri() . '/lib/plugins/sw_core.zip',  
        'required'  		 => true,   
        'version'			 => '1.3.2'
        ),

      array(
        'name'     			 => esc_html__( 'SW WooCommerce', 'shop4u' ),
        'slug'      		 => 'sw_woocommerce',
        'source'             => get_template_directory_uri() . '/lib/plugins/sw_woocommerce.zip',  
        'required'  		 => true,
        'version'			 => '1.5.4'
        ),

      array(
        'name'     			=> esc_html__( 'SW Ajax Woocommerce Search', 'shop4u' ),
        'slug'      		=> 'sw_ajax_woocommerce_search',
       'source'             => get_template_directory_uri() . '/lib/plugins/sw_ajax_woocommerce_search.zip',  
        'required'  		=> true,
        'version'			=> '1.1.6'
        ),

      array(
        'name'     			 => esc_html__( 'SW Wooswatches', 'shop4u' ),
        'slug'      		 => 'sw_wooswatches',
        'source'             => get_template_directory_uri() . '/lib/plugins/sw_wooswatches.zip',  
        'required'  		 => true,
        'version'			 => '1.0.6'
        ),

      array(
        'name'               => esc_html__( 'One Click Install', 'shop4u' ), 
        'slug'               => 'one-click-demo-import', 
        'source'             => get_template_directory_uri() . '/lib/plugins/one-click-demo-import.zip',  
        'required'           => true, 
        ),
	
      array(
        'name'      		 => esc_html__( 'MailChimp for WordPress Lite', 'shop4u' ),
        'slug'     			 => 'mailchimp-for-wp',
        'required' 			 => false,
        ),
      array(
        'name'      		 => esc_html__( 'Contact Form 7', 'shop4u' ),
        'slug'     			 => 'contact-form-7',
        'required' 			 => false,
        ),
      array(
        'name'      		 => esc_html__( 'YITH Woocommerce Compare', 'shop4u' ),
        'slug'      		 => 'yith-woocommerce-compare',
        'required'			 => false
        ),
      array(
        'name'     			 => esc_html__( 'YITH Woocommerce Wishlist', 'shop4u' ),
        'slug'      		 => 'yith-woocommerce-wishlist',
        'required' 			 => false
        ), 
      array(
        'name'     			 => esc_html__( 'WordPress Seo', 'shop4u' ),
        'slug'      		 => 'wordpress-seo',
        'required'  		 => false,
        ),

      );		
$config = array();

tgmpa( $plugins, $config );

}
add_action( 'vc_before_init', 'shop4u_vcSetAsTheme' );
function shop4u_vcSetAsTheme() {
    vc_set_as_theme();
}