<?php
$lib_dir = trailingslashit( str_replace( '\\', '/', get_template_directory() . '/lib/' ) );

if( !defined('SHOP4U_DIR') ){
	define( 'SHOP4U_DIR', $lib_dir );
}

if( !defined('SHOP4U_URL') ){
	define( 'SHOP4U_URL', trailingslashit( get_template_directory_uri() ) . 'lib' );
}

if (!isset($content_width)) { $content_width = 940; }

define("SHOP4U_PRODUCT_TYPE","product");
define("SHOP4U_PRODUCT_DETAIL_TYPE","product_detail");

if ( !defined('SW_THEME') ){
	define( 'SW_THEME', 'shop4u_theme' );
}
require_once( get_template_directory().'/lib/options.php' );

if( class_exists( 'SW_Options' ) ) :
	function shop4u_Options_Setup(){
		global $sw_options, $options, $options_args;

		$options = array();
		$options[] = array(
			'title' => esc_html__('General', 'shop4u'),
			'desc' => wp_kses( __('<p class="description">The theme allows to build your own styles right out of the backend without any coding knowledge. Upload new logo and favicon or get their URL.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
			'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_019_cogwheel.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
			'fields' => array(	

				array(
					'id' => 'sitelogo',
					'type' => 'upload',
					'title' => esc_html__('Logo Image', 'shop4u'),
					'sub_desc' => esc_html__( 'Use the Upload button to upload the new logo and get URL of the logo', 'shop4u' ),
					'std' => get_template_directory_uri().'/assets/img/logo-default.png'
					),

				array(
					'id' => 'favicon',
					'type' => 'upload',
					'title' => esc_html__('Favicon', 'shop4u'),
					'sub_desc' => esc_html__( 'Use the Upload button to upload the custom favicon', 'shop4u' ),
					'std' => ''
					),

				array(
					'id' => 'tax_select',
					'type' => 'multi_select_taxonomy',
					'title' => esc_html__('Select Taxonomy', 'shop4u'),
					'sub_desc' => esc_html__( 'Select taxonomy to show custom term metabox', 'shop4u' ),
					),

				array(
					'id' => 'title_length',
					'type' => 'text',
					'title' => esc_html__('Title Length Of Item Listing Page', 'shop4u'),
					'sub_desc' => esc_html__( 'Choose title length if you want to trim word, leave 0 to not trim word', 'shop4u' ),
					'std' => 0
					)					
				)		
);

$options[] = array(
	'title' => esc_html__('Schemes', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Custom color scheme for theme. Unlimited color that you can choose.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_163_iphone.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(		
		array(
			'id' => 'scheme',
			'type' => 'radio_img',
			'title' => esc_html__('Color Scheme', 'shop4u'),
			'sub_desc' => esc_html__( 'Select one of 2 predefined schemes', 'shop4u' ),
			'desc' => '',
			'options' => array(
					'default' => array('title' => 'Default', 'img' => get_template_directory_uri().'/assets/img/default.png'),
					'pink' => array('title' => 'Pink', 'img' => get_template_directory_uri().'/assets/img/pink.png'),
				), //Must provide key => value(array:title|img) pairs for radio options
			'std' => 'pink'
			),
		
		array(
			'id' => 'custom_color',
			'title' => esc_html__( 'Enable Custom Color', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Check this field to enable custom color and when you update your theme, custom color will not lose.', 'shop4u' ),
			'desc' => '',
			'std' => '0'
		),

		array(
			'id' => 'developer_mode',
			'title' => esc_html__( 'Developer Mode', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Turn on/off compile less to css and custom color', 'shop4u' ),
			'desc' => '',
			'std' => '0'
			),

		array(
			'id' => 'scheme_color',
			'type' => 'color',
			'title' => esc_html__('Color', 'shop4u'),
			'sub_desc' => esc_html__('Select main custom color.', 'shop4u'),
			'std' => ''
			),

		array(
			'id' => 'scheme_body',
			'type' => 'color',
			'title' => esc_html__('Body Color', 'shop4u'),
			'sub_desc' => esc_html__('Select main body custom color.', 'shop4u'),
			'std' => ''
			),
		
		)
);

$options[] = array(
	'title' => esc_html__('Layout', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">WpThemeGo Framework comes with a layout setting that allows you to build any number of stunning layouts and apply theme to your entries.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_319_sort.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'layout',
			'type' => 'select',
			'title' => esc_html__('Box Layout', 'shop4u'),
			'sub_desc' => esc_html__( 'Select Layout Box or Wide', 'shop4u' ),
			'options' => array(
				'full' => esc_html__( 'Default', 'shop4u' ),
				'boxed' => esc_html__( 'Boxed', 'shop4u' ),
				),
			'std' => 'full'
			),

		array(
			'id' => 'bg_box_img',
			'type' => 'upload',
			'title' => esc_html__('Background Box Image', 'shop4u'),
			'sub_desc' => '',
			'std' => ''
			),
		array(
			'id' => 'sidebar_left_expand',
			'type' => 'select',
			'title' => esc_html__('Left Sidebar Expand', 'shop4u'),
			'options' => array(
				'2' => '2/12',
				'3' => '3/12',
				'4' => '4/12',
				'5' => '5/12', 
				'6' => '6/12',
				'7' => '7/12',
				'8' => '8/12',
				'9' => '9/12',
				'10' => '10/12',
				'11' => '11/12',
				'12' => '12/12'
				),
			'std' => '3',
			'sub_desc' => esc_html__( 'Select width of left sidebar.', 'shop4u' ),
			),

		array(
			'id' => 'sidebar_right_expand',
			'type' => 'select',
			'title' => esc_html__('Right Sidebar Expand', 'shop4u'),
			'options' => array(
				'2' => '2/12',
				'3' => '3/12',
				'4' => '4/12',
				'5' => '5/12',
				'6' => '6/12',
				'7' => '7/12',
				'8' => '8/12',
				'9' => '9/12',
				'10' => '10/12',
				'11' => '11/12',
				'12' => '12/12'
				),
			'std' => '3',
			'sub_desc' => esc_html__( 'Select width of right sidebar medium desktop.', 'shop4u' ),
			),
		array(
			'id' => 'sidebar_left_expand_md',
			'type' => 'select',
			'title' => esc_html__('Left Sidebar Medium Desktop Expand', 'shop4u'),
			'options' => array(
				'2' => '2/12',
				'3' => '3/12',
				'4' => '4/12',
				'5' => '5/12',
				'6' => '6/12',
				'7' => '7/12',
				'8' => '8/12',
				'9' => '9/12',
				'10' => '10/12',
				'11' => '11/12',
				'12' => '12/12'
				),
			'std' => '4',
			'sub_desc' => esc_html__( 'Select width of left sidebar medium desktop.', 'shop4u' ),
			),
		array(
			'id' => 'sidebar_right_expand_md',
			'type' => 'select',
			'title' => esc_html__('Right Sidebar Medium Desktop Expand', 'shop4u'),
			'options' => array(
				'2' => '2/12',
				'3' => '3/12',
				'4' => '4/12',
				'5' => '5/12',
				'6' => '6/12',
				'7' => '7/12',
				'8' => '8/12',
				'9' => '9/12',
				'10' => '10/12',
				'11' => '11/12',
				'12' => '12/12'
				),
			'std' => '4',
			'sub_desc' => esc_html__( 'Select width of right sidebar.', 'shop4u' ),
			),
		array(
			'id' => 'sidebar_left_expand_sm',
			'type' => 'select',
			'title' => esc_html__('Left Sidebar Tablet Expand', 'shop4u'),
			'options' => array(
				'2' => '2/12',
				'3' => '3/12',
				'4' => '4/12',
				'5' => '5/12',
				'6' => '6/12',
				'7' => '7/12',
				'8' => '8/12',
				'9' => '9/12',
				'10' => '10/12',
				'11' => '11/12',
				'12' => '12/12'
				),
			'std' => '4',
			'sub_desc' => esc_html__( 'Select width of left sidebar tablet.', 'shop4u' ),
			),
		array(
			'id' => 'sidebar_right_expand_sm',
			'type' => 'select',
			'title' => esc_html__('Right Sidebar Tablet Expand', 'shop4u'),
			'options' => array(
				'2' => '2/12',
				'3' => '3/12',
				'4' => '4/12',
				'5' => '5/12',
				'6' => '6/12',
				'7' => '7/12',
				'8' => '8/12',
				'9' => '9/12',
				'10' => '10/12',
				'11' => '11/12',
				'12' => '12/12'
				),
			'std' => '4',
			'sub_desc' => esc_html__( 'Select width of right sidebar tablet.', 'shop4u' ),
			),				
		)
);
$options[] = array(
	'title' => esc_html__('Mobile Layout', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">WpThemeGo Framework comes with a mobile setting home page layout.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_163_iphone.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(				
		array(
			'id' => 'mobile_enable',
			'type' => 'checkbox',
			'title' => esc_html__('Enable Mobile Layout', 'shop4u'),
			'sub_desc' => '',
			'desc' => '',
					'std' => '1'// 1 = on | 0 = off
					),

		array(
			'id' => 'mobile_logo',
			'type' => 'upload',
			'title' => esc_html__('Logo Mobile Image', 'shop4u'),
			'sub_desc' => esc_html__( 'Use the Upload button to upload the new mobile logo', 'shop4u' ),
			'std' => get_template_directory_uri().'/assets/img/logo-default.png'
			),

		array(
			'id' => 'sticky_mobile',
			'type' => 'checkbox',
			'title' => esc_html__('Sticky Mobile', 'shop4u'),
			'sub_desc' => '',
			'desc' => '',
					'std' => '0'// 1 = on | 0 = off
					),

		array(
			'id' => 'mobile_content',
			'type' => 'pages_select',
			'title' => esc_html__('Mobile Layout Content', 'shop4u'),
			'sub_desc' => esc_html__('Select content index for this mobile layout', 'shop4u'),
			'std' => ''
			),

		array(
			'id' => 'mobile_header_style',
			'type' => 'select',
			'title' => esc_html__('Header Mobile Style', 'shop4u'),
			'sub_desc' => esc_html__('Select header mobile style', 'shop4u'),
			'options' => array(
				'mstyle1'  => esc_html__( 'Style 1', 'shop4u' ),
				),
			'std' => 'style1'
			),

		array(
			'id' => 'mobile_footer_style',
			'type' => 'select',
			'title' => esc_html__('Footer Mobile Style', 'shop4u'),
			'sub_desc' => esc_html__('Select footer mobile style', 'shop4u'),
			'options' => array(
				'mstyle1'  => esc_html__( 'Style 1', 'shop4u' ),
				),
			'std' => 'style1'
			)				
		)
);

$options[] = array(
	'title' => esc_html__('Header & Footer', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">WpThemeGo Framework comes with a header and footer setting that allows you to build style header.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_336_read_it_later.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'header_style',
			'type' => 'select',
			'title' => esc_html__('Header Style', 'shop4u'),
			'sub_desc' => esc_html__('Select Header style', 'shop4u'),
			'options' => array(
				'style1'  => esc_html__( 'Style 1', 'shop4u' ),
				'style2'  => esc_html__( 'Style 2', 'shop4u' ),
				),
			'std' => 'style2'
			),

		array(
			'id' => 'disable_search',
			'title' => esc_html__( 'Disable Search', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Check this to disable search on header', 'shop4u' ),
			'desc' => '',
			'std' => '0'
			),

		array(
			'id' => 'disable_cart',
			'title' => esc_html__( 'Disable Cart', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Check this to disable cart on header', 'shop4u' ),
			'desc' => '',
			'std' => '0'
			),				

		array(
			'id' => 'footer_style',
			'type' => 'pages_select',
			'title' => esc_html__('Footer Style', 'shop4u'),
			'sub_desc' => esc_html__('Select Footer style', 'shop4u'),
			'std' => ''
			),

		array(
			'id' => 'copyright_style',
			'type' => 'select',
			'title' => esc_html__('Copyright Style', 'shop4u'),
			'sub_desc' => esc_html__('Select Copyright style', 'shop4u'),
			'options' => array(
				'style1'  => esc_html__( 'Style 1', 'shop4u' ),
				'style2'  => esc_html__( 'Style 2', 'shop4u' ),
				),
			'std' => 'style1'
			),

		array(
			'id' => 'footer_copyright',
			'type' => 'editor',
			'sub_desc' => '',
			'title' => esc_html__( 'Copyright text', 'shop4u' )
			),	

		)
);
$options[] = array(
	'title' => esc_html__('Navbar Options', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">If you got a big site with a lot of sub menus we recommend using a mega menu. Just select the dropbox to display a menu as mega menu or dropdown menu.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_157_show_lines.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'menu_type',
			'type' => 'select',
			'title' => esc_html__('Menu Type', 'shop4u'),
			'options' => array( 'dropdown' => 'Dropdown Menu', 'mega' => 'Mega Menu' ),
			'std' => 'mega'
			),

		array(
			'id' => 'menu_location',
			'type' => 'menu_location_multi_select',
			'title' => esc_html__('Theme Location', 'shop4u'),
			'sub_desc' => esc_html__( 'Select theme location to active mega menu and menu responsive.', 'shop4u' ),
			'std' => 'primary_menu'
			),		

		array(
			'id' => 'sticky_menu',
			'type' => 'checkbox',
			'title' => esc_html__('Active sticky menu', 'shop4u'),
			'sub_desc' => '',
			'desc' => '',
						'std' => '0'// 1 = on | 0 = off
						),

		array(
			'id' => 'menu_event',
			'type' => 'select',
			'title' => esc_html__('Menu Event', 'shop4u'),
			'options' => array( '' => esc_html__( 'Hover Event', 'shop4u' ), 'click' => esc_html__( 'Click Event', 'shop4u' ) ),
			'std' => ''
			),

		array(
			'id' => 'menu_number_item',
			'type' => 'text',
			'title' => esc_html__( 'Number Item Vertical', 'shop4u' ),
			'sub_desc' => esc_html__( 'Number item vertical to show', 'shop4u' ),
			'std' => 8
			),	

		array(
			'id' => 'menu_title_text',
			'type' => 'text',
			'title' => esc_html__('Vertical Title Text', 'shop4u'),
			'sub_desc' => esc_html__( 'Change title text on vertical menu', 'shop4u' ),
			'std' => ''
			),
		
		array(
			'id' => 'menu_more_text',
			'type' => 'text',
			'title' => esc_html__('Vertical More Text', 'shop4u'),
			'sub_desc' => esc_html__( 'Change more text on vertical menu', 'shop4u' ),
			'std' => ''
			),

		array(
			'id' => 'menu_less_text',
			'type' => 'text',
			'title' => esc_html__('Vertical Less Text', 'shop4u'),
			'sub_desc' => esc_html__( 'Change less text on vertical menu', 'shop4u' ),
			'std' => ''
			)	
		)
);
$options[] = array(
	'title' => esc_html__('Blog Options', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Select layout in blog listing page.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_071_book.png',
		//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'sidebar_blog',
			'type' => 'select',
			'title' => esc_html__('Sidebar Blog Layout', 'shop4u'),
			'options' => array(
				'full' => esc_html__( 'Full Layout', 'shop4u' ),		
				'left'	=>  esc_html__( 'Left Sidebar', 'shop4u' ),
				'right' => esc_html__( 'Right Sidebar', 'shop4u' ),
				),
			'std' => 'left',
			'sub_desc' => esc_html__( 'Select style sidebar blog', 'shop4u' ),
			),
		array(
			'id' => 'blog_layout',
			'type' => 'select',
			'title' => esc_html__('Layout blog', 'shop4u'),
			'options' => array(
				'list'	=>  esc_html__( 'List Layout', 'shop4u' ),
				'grid' =>  esc_html__( 'Grid Layout', 'shop4u' )								
				),
			'std' => 'list',
			'sub_desc' => esc_html__( 'Select style layout blog', 'shop4u' ),
			),
		array(
			'id' => 'blog_column',
			'type' => 'select',
			'title' => esc_html__('Blog column', 'shop4u'),
			'options' => array(								
				'2' => '2 columns',
				'3' => '3 columns',
				'4' => '4 columns'								
				),
			'std' => '2',
			'sub_desc' => esc_html__( 'Select style number column blog', 'shop4u' ),
			),
		)
);	
$options[] = array(
	'title' => esc_html__('Product Options', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Select layout in product listing page.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_202_shopping_cart.png',
		//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'product_banner',
			'title' => esc_html__( 'Select Banner', 'shop4u' ),
			'type' => 'select',
			'sub_desc' => '',
			'options' => array(
				'' => esc_html__( 'Use Banner', 'shop4u' ),
				'listing' => esc_html__( 'Use Category Product Image', 'shop4u' ),
				),
			'std' => '',
			),

		array(
			'id' => 'product_listing_banner',
			'type' => 'upload',
			'title' => esc_html__('Listing Banner Product', 'shop4u'),
			'sub_desc' => esc_html__( 'Use the Upload button to upload banner product listing', 'shop4u' ),
			'std' => get_template_directory_uri().'/assets/img/listing-banner.jpg'
			),

		array(
			'id' => 'product_col_large',
			'type' => 'select',
			'title' => esc_html__('Product Listing column Desktop', 'shop4u'),
			'options' => array(
				'2' => '2',
				'3' => '3',
				'4' => '4',							
				),
			'std' => '3',
			'sub_desc' => esc_html__( 'Select number of column on Desktop Screen', 'shop4u' ),
			),

		array(
			'id' => 'product_col_medium',
			'type' => 'select',
			'title' => esc_html__('Product Listing column Medium Desktop', 'shop4u'),
			'options' => array(
				'2' => '2',
				'3' => '3',
				'4' => '4',							
				),
			'std' => '2',
			'sub_desc' => esc_html__( 'Select number of column on Medium Desktop Screen', 'shop4u' ),
			),

		array(
			'id' => 'product_col_sm',
			'type' => 'select',
			'title' => esc_html__('Product Listing column Tablet', 'shop4u'),
			'options' => array(
				'2' => '2',
				'3' => '3',
				'4' => '4',							
				),
			'std' => '2',
			'sub_desc' => esc_html__( 'Select number of column on Tablet Screen', 'shop4u' ),
			),

		array(
			'id' => 'sidebar_product',
			'type' => 'select',
			'title' => esc_html__('Sidebar Product Layout', 'shop4u'),
			'options' => array(
				'left'	=> esc_html__( 'Left Sidebar', 'shop4u' ),
				'full' => esc_html__( 'Full Layout', 'shop4u' ),		
				'right' => esc_html__( 'Right Sidebar', 'shop4u' )
				),
			'std' => 'left',
			'sub_desc' => esc_html__( 'Select style sidebar product', 'shop4u' ),
			),

		array(
			'id' => 'product_quickview',
			'title' => esc_html__( 'Quickview', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => '',
			'desc' => esc_html__( 'Turn On/Off Product Quickview', 'shop4u' ),
			'std' => '1'
			),

		array(
			'id' => 'product_zoom',
			'title' => esc_html__( 'Product Zoom', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => '',
			'desc' => esc_html__( 'Turn On/Off image zoom when hover on single product', 'shop4u' ),
			'std' => '1'
			),

		array(
			'id' => 'product_brand',
			'title' => esc_html__( 'Enable Product Brand Image', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => '',
			'desc' => esc_html__( 'Turn On/Off product brand image show on single product.', 'shop4u' ),
			'std' => '1'
			),

		array(
			'id' => 'product_number',
			'type' => 'text',
			'title' => esc_html__('Product Listing Number', 'shop4u'),
			'sub_desc' => esc_html__( 'Show number of product in listing product page.', 'shop4u' ),
			'std' => 12
			),

		array(
			'id' => 'info_typo1',
			'type' => 'info',
			'title' => esc_html( 'Config For Product Categories Widget', 'shop4u' ),
			'desc' => '',
			'class' => 'shop4u-opt-info'
			),

		array(
			'id' => 'product_number_item',
			'type' => 'text',
			'title' => esc_html__( 'Category Number Item Show', 'shop4u' ),
			'sub_desc' => esc_html__( 'Choose to number of item category that you want to show, leave 0 to show all category', 'shop4u' ),
			'std' => 8
			),	

		array(
			'id' => 'product_more_text',
			'type' => 'text',
			'title' => esc_html__( 'Category More Text', 'shop4u' ),
			'sub_desc' => esc_html__( 'Change more text on category product', 'shop4u' ),
			'std' => ''
			),

		array(
			'id' => 'product_less_text',
			'type' => 'text',
			'title' => esc_html__( 'Category Less Text', 'shop4u' ),
			'sub_desc' => esc_html__( 'Change less text on category product', 'shop4u' ),
			'std' => ''
			)	
		)
);		
$options[] = array(
	'title' => esc_html__('Typography', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Change the font style of your blog, custom with Google Font.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_151_edit.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'info_typo1',
			'type' => 'info',
			'title' => esc_html( 'Global Typography', 'shop4u' ),
			'desc' => '',
			'class' => 'shop4u-opt-info'
			),

		array(
			'id' => 'google_webfonts',
			'type' => 'google_webfonts',
			'title' => esc_html__('Use Google Webfont', 'shop4u'),
			'sub_desc' => esc_html__( 'Insert font style that you actually need on your webpage.', 'shop4u' ), 
			'std' => ''
			),

		array(
			'id' => 'webfonts_weight',
			'type' => 'multi_select',
			'sub_desc' => esc_html__( 'For weight, see Google Fonts to custom for each font style.', 'shop4u' ),
			'title' => esc_html__('Webfont Weight', 'shop4u'),
			'options' => array(
				'100' => '100',
				'200' => '200',
				'300' => '300',
				'400' => '400',
				'500' => '500',
				'600' => '600',
				'700' => '700',
				'800' => '800',
				'900' => '900'
				),
			'std' => ''
			),

		array(
			'id' => 'info_typo2',
			'type' => 'info',
			'title' => esc_html( 'Header Tag Typography', 'shop4u' ),
			'desc' => '',
			'class' => 'shop4u-opt-info'
			),

		array(
			'id' => 'header_tag_font',
			'type' => 'google_webfonts',
			'title' => esc_html__('Header Tag Font', 'shop4u'),
			'sub_desc' => esc_html__( 'Select custom font for header tag ( h1...h6 )', 'shop4u' ), 
			'std' => ''
			),

		array(
			'id' => 'info_typo2',
			'type' => 'info',
			'title' => esc_html( 'Main Menu Typography', 'shop4u' ),
			'desc' => '',
			'class' => 'shop4u-opt-info'
			),

		array(
			'id' => 'menu_font',
			'type' => 'google_webfonts',
			'title' => esc_html__('Main Menu Font', 'shop4u'),
			'sub_desc' => esc_html__( 'Select custom font for main menu', 'shop4u' ), 
			'std' => ''
			),

		)
);

$options[] = array(
	'title' => __('Social', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">This feature allow to you link to your social.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_222_share.png',
		//Lets leave this as a blank section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'social-share-fb',
			'title' => esc_html__( 'Facebook', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			),
		array(
			'id' => 'social-share-tw',
			'title' => esc_html__( 'Twitter', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			),
		array(
			'id' => 'social-share-tumblr',
			'title' => esc_html__( 'Tumblr', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			),
		array(
			'id' => 'social-share-in',
			'title' => esc_html__( 'Linkedin', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			),
		array(
			'id' => 'social-share-instagram',
			'title' => esc_html__( 'Instagram', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			),
		array(
			'id' => 'social-share-go',
			'title' => esc_html__( 'Google+', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			),
		array(
			'id' => 'social-share-pi',
			'title' => esc_html__( 'Pinterest', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => '',
			'desc' => '',
			'std' => '',
			)

		)
);

$options[] = array(
	'title' => esc_html__('Maintaincece Mode', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Enable and config for Maintaincece mode.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_136_computer_locked.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'maintaince_enable',
			'title' => esc_html__( 'Enable Maintaincece Mode', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Turn on/off Maintaince mode on this website', 'shop4u' ),
			'desc' => '',
			'std' => '0'
			),

		array(
			'id' => 'maintaince_background',
			'title' => esc_html__( 'Maintaince Background', 'shop4u' ),
			'type' => 'upload',
			'sub_desc' => esc_html__( 'Choose maintance background image', 'shop4u' ),
			'desc' => '',
			'std' => get_template_directory_uri().'/assets/img/maintaince/bg-main.jpg'
			),

		array(
			'id' => 'maintaince_content',
			'title' => esc_html__( 'Maintaince Content', 'shop4u' ),
			'type' => 'editor',
			'sub_desc' => esc_html__( 'Change text of maintaince mode', 'shop4u' ),
			'desc' => '',
			'std' => ''
			),

		array(
			'id' => 'maintaince_date',
			'title' => esc_html__( 'Maintaince Date', 'shop4u' ),
			'type' => 'date',
			'sub_desc' => esc_html__( 'Put date to this field to show countdown date on maintaince mode.', 'shop4u' ),
			'desc' => '',
			'placeholder' => 'mm/dd/yy',
			'std' => ''
			),

		array(
			'id' => 'maintaince_form',
			'title' => esc_html__( 'Maintaince Form', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => esc_html__( 'Put shortcode form to this field and it will be shown on maintaince mode frontend.', 'shop4u' ),
			'desc' => '',
			'std' => ''
			),

		)
);

$options[] = array(
	'title' => esc_html__('Popup Config', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Enable popup and more config for Popup.</p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_318_more-items.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'popup_active',
			'type' => 'checkbox',
			'title' => esc_html__( 'Active Popup Subscribe', 'shop4u' ),
			'sub_desc' => esc_html__( 'Check to active popup subscribe', 'shop4u' ),
			'desc' => '',
						'std' => '0'// 1 = on | 0 = off
						),	

		array(
			'id' => 'popup_background',
			'title' => esc_html__( 'Popup Background', 'shop4u' ),
			'type' => 'upload',
			'sub_desc' => esc_html__( 'Choose popup background image', 'shop4u' ),
			'desc' => '',
			'std' => get_template_directory_uri().'/assets/img/popup/bg-main.jpg'
			),

		array(
			'id' => 'popup_content',
			'title' => esc_html__( 'Popup Content', 'shop4u' ),
			'type' => 'editor',
			'sub_desc' => esc_html__( 'Change text of popup mode', 'shop4u' ),
			'desc' => '',
			'std' => ''
			),	

		array(
			'id' => 'popup_form',
			'title' => esc_html__( 'Popup Form', 'shop4u' ),
			'type' => 'text',
			'sub_desc' => esc_html__( 'Put shortcode form to this field and it will be shown on popup mode frontend.', 'shop4u' ),
			'desc' => '',
			'std' => ''
			),

		)
);

$options[] = array(
	'title' => esc_html__('Advanced', 'shop4u'),
	'desc' => wp_kses( __('<p class="description">Custom advanced with Cpanel, Widget advanced, Developer mode </p>', 'shop4u'), array( 'p' => array( 'class' => array() ) ) ),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it shop4u for default.
	'icon' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_083_random.png',
			//Lets leave this as a shop4u section, no options just some intro text set above.
	'fields' => array(
		array(
			'id' => 'show_cpanel',
			'title' => esc_html__( 'Show cPanel', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Turn on/off Cpanel', 'shop4u' ),
			'desc' => '',
			'std' => ''
			),

		array(
			'id' => 'widget-advanced',
			'title' => esc_html__('Widget Advanced', 'shop4u'),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Turn on/off Widget Advanced', 'shop4u' ),
			'desc' => '',
			'std' => '1'
			),					

		array(
			'id' => 'social_share',
			'title' => esc_html__( 'Social Share', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Turn on/off social share', 'shop4u' ),
			'desc' => '',
			'std' => '1'
			),

		array(
			'id' => 'breadcrumb_active',
			'title' => esc_html__( 'Turn Off Breadcrumb', 'shop4u' ),
			'type' => 'checkbox',
			'sub_desc' => esc_html__( 'Turn off breadcumb on all page', 'shop4u' ),
			'desc' => '',
			'std' => '0'
			),

		array(
			'id' => 'back_active',
			'type' => 'checkbox',
			'title' => esc_html__('Back to top', 'shop4u'),
			'sub_desc' => '',
			'desc' => '',
						'std' => '1'// 1 = on | 0 = off
						),	

		array(
			'id' => 'direction',
			'type' => 'select',
			'title' => esc_html__('Direction', 'shop4u'),
			'options' => array( 'ltr' => 'Left to Right', 'rtl' => 'Right to Left' ),
			'std' => 'ltr'
			),

		)
);

$options_args = array();

	//Setup custom links in the footer for share icons
$options_args['share_icons']['facebook'] = array(
	'link' => 'http://www.facebook.com/WpThemeGo.page',
	'title' => 'Facebook',
	'img' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_320_facebook.png'
	);
$options_args['share_icons']['twitter'] = array(
	'link' => 'https://twitter.com/WpThemeGo',
	'title' => 'Folow me on Twitter',
	'img' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_322_twitter.png'
	);
$options_args['share_icons']['linked_in'] = array(
	'link' => 'http://www.linkedin.com/in/WpThemeGo',
	'title' => 'Find me on LinkedIn',
	'img' => SHOP4U_URL.'/admin/img/glyphicons/glyphicons_337_linked_in.png'
	);


	//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
$options_args['opt_name'] = SW_THEME;

	$options_args['google_api_key'] = 'AIzaSyDSOIEOAvc2w0UwX16lKUXtz0AQ2G9K7dM'; //must be defined for use with google webfonts field type

	//Custom menu title for options page - default is "Options"
	$options_args['menu_title'] = esc_html__('Theme Options', 'shop4u');

	//Custom Page Title for options page - default is "Options"
	$options_args['page_title'] = esc_html__('Shop4u Options ', 'shop4u') . wp_get_theme()->get('Name');

	//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "shop4u_theme_options"
	$options_args['page_slug'] = 'shop4u_theme_options';

	//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
	$options_args['page_type'] = 'submenu';

	//custom page location - default 100 - must be unique or will override other items
	$options_args['page_position'] = 27;
	$sw_options = new SW_Options( $options, $options_args );
}
add_action( 'init', 'shop4u_Options_Setup', 0 );
// shop4u_Options_Setup();
endif; 


/*
** Define widget
*/
function shop4u_widget_setup_args(){
	$shop4u_widget_areas = array(
		
		array(
			'name' => esc_html__('Sidebar Left Blog', 'shop4u'),
			'id'   => 'left-blog',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
			),
		array(
			'name' => esc_html__('Sidebar Right Blog', 'shop4u'),
			'id'   => 'right-blog',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
			),		
		array(
			'name' => esc_html__('Header Top', 'shop4u'),
			'id'   => 'top',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		array(
			'name' => esc_html__('Header Top2', 'shop4u'),
			'id'   => 'top2',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		array(
			'name' => esc_html__('Header Mid', 'shop4u'),
			'id'   => 'mid-header',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		array(
			'name' => esc_html__('Header Mid2', 'shop4u'),
			'id'   => 'mid-header2',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		array(
			'name' => esc_html__('Header Bottom', 'shop4u'),
			'id'   => 'bottom-header',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		array(
			'name' => esc_html__('Header Bottom2', 'shop4u'),
			'id'   => 'bottom-header2',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),

		
		array(
			'name' => esc_html__('Sidebar Left Product', 'shop4u'),
			'id'   => 'left-product',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
			),		
		array(
			'name' => esc_html__('Sidebar Right Product', 'shop4u'),
			'id'   => 'right-product',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
			),		
		array(
			'name' => esc_html__('Sidebar Left Detail Product', 'shop4u'),
			'id'   => 'left-product-detail',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
			),		
		array(
			'name' => esc_html__('Sidebar Right Detail Product', 'shop4u'),
			'id'   => 'right-product-detail',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
			),		
		array(
			'name' => esc_html__('Sidebar Bottom Detail Product', 'shop4u'),
			'id'   => 'bottom-detail-product',
			'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		array(
			'name' => esc_html__('Bottom Detail Product Mobile', 'shop4u'),
			'id'   => 'bottom-detail-product-mobile',
			'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		),

		array(
			'name' => esc_html__('Filter Mobile', 'shop4u'),
			'id'   => 'filter-mobile',
			'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		),	
		array(
			'name' => esc_html__('Footer Copyright', 'shop4u'),
			'id'   => 'footer-copyright',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
			),
		);
return apply_filters( 'shop4u_widget_register', $shop4u_widget_areas );
}