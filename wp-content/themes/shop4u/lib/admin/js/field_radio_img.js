/*
 *
 * SW_Options_radio_img function
 * Changes the radio select option, and changes class on images
 *
 */
function shop4u_radio_img_select(relid, labelclass){
	jQuery(this).prev('input[type="radio"]').prop('checked');

	jQuery('.shop4u-radio-img-'+labelclass).removeClass('shop4u-radio-img-selected');	
	
	jQuery('label[for="'+relid+'"]').addClass('shop4u-radio-img-selected');
}//function