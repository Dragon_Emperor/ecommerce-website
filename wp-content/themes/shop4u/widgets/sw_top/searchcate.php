<?php if( !sw_options( 'disable_search' ) ) : ?>
	<div class="top-form top-search">
		<div class="topsearch-entry">
			<?php if ( class_exists( 'WooCommerce' ) ) { ?>
			<form method="get" id="searchform_special" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
				<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php esc_attr_e( 'Enter your keyword...', 'shop4u' ); ?>" />
				<button type="submit" title="<?php esc_attr_e( 'Search', 'shop4u' ) ?>" class="fa fa-search button-search-pro form-button"></button>
				<input type="hidden" name="search_posttype" value="product" />
			</form>
			<?php }else{ ?>
			<?php get_template_part('templates/searchform'); ?>
			<?php } ?>
		</div>
	</div>
	<?php 
	$shop4u_psearch = sw_options('popular_search'); 
	if( $shop4u_psearch != '' ){
		?>
		<div class="popular-search-keyword">
			<div class="keyword-title"><?php esc_html_e( 'Popular Keywords', 'shop4u' ) ?>: </div>
			<?php 
			$shop4u_psearch = explode(',', $shop4u_psearch); 
			foreach( $shop4u_psearch as $key => $item ){
				echo ( $key == 0 ) ? '' : ',';
				echo '<a href="'. esc_url( home_url('/') ) .'?s='. esc_attr( $item ) .'">' . $item . '</a>';
			}
			?>		
		</div>
	<?php } ?>
<?php endif; ?>
