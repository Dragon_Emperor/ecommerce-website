<?php get_header(); ?>
<?php 
	$shop4u_sidebar_template	= get_post_meta( get_the_ID(), 'page_sidebar_layout', true );
	$shop4u_sidebar 					= get_post_meta( get_the_ID(), 'page_sidebar_template', true );
?>

	<div class="shop4u_breadcrumbs">
		<div class="container">
			<?php
				if (!is_front_page() ) {
					if (function_exists('shop4u_breadcrumb')){
						shop4u_breadcrumb('<div class="breadcrumbs custom-font theme-clearfix">', '</div>');
					} 
				} 
			?>
		</div>
	</div>	
	<div class="container">
		<div class="row">
		<?php 
			if ( is_active_sidebar( $shop4u_sidebar ) && $shop4u_sidebar_template != 'right' && $shop4u_sidebar_template !='full' ):
			$shop4u_left_span_class = 'col-lg-'.sw_options('sidebar_left_expand');
			$shop4u_left_span_class .= ' col-md-'.sw_options('sidebar_left_expand_md');
			$shop4u_left_span_class .= ' col-sm-'.sw_options('sidebar_left_expand_sm');
		?>
			<aside id="left" class="sidebar <?php echo esc_attr( $shop4u_left_span_class ); ?>">
				<?php dynamic_sidebar( $shop4u_sidebar ); ?>
			</aside>
		<?php endif; ?>
		
			<div id="contents" role="main" class="main-page <?php shop4u_content_page(); ?>">
				<?php
				get_template_part('templates/content', 'page')
				?>
			</div>
			<?php 
			if ( is_active_sidebar( $shop4u_sidebar ) && $shop4u_sidebar_template != 'left' && $shop4u_sidebar_template !='full' ):
				$shop4u_left_span_class = 'col-lg-'.sw_options('sidebar_left_expand');
				$shop4u_left_span_class .= ' col-md-'.sw_options('sidebar_left_expand_md');
				$shop4u_left_span_class .= ' col-sm-'.sw_options('sidebar_left_expand_sm');
			?>
				<aside id="right" class="sidebar <?php echo esc_attr($shop4u_left_span_class); ?>">
					<?php dynamic_sidebar( $shop4u_sidebar ); ?>
				</aside>
			<?php endif; ?>
		</div>		
	</div>
<?php get_footer(); ?>

