<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'pjFNG&&gQ#0L1] q)Q?XqbLC695:qn]&4`&r+nrvO-?z9N(smwW7fHC+o:Sd2niA' );
define( 'SECURE_AUTH_KEY',  'dXP%9^P9L1k><=fQ[BWj Sq]Wgg]VnD:AwIEIjk-BYeBKFa:F,z!aSKDcsrgU/q6' );
define( 'LOGGED_IN_KEY',    'iekG#8Y%N?3/op}Pf$Z@_e(uTiL@}4GERS%v3A+zU`[joC$p_SFjDj3DU!&WOQMl' );
define( 'NONCE_KEY',        '|m/AHFFGlezCGB@8|.9fZ9Ef5V& ]05NnOLw,[l.co-ADLQ{Z}jLy=hw1O<yOu:A' );
define( 'AUTH_SALT',        'S|D,3b#N=ue,_zz5>WBmm/Z-OVF>cXTNP#irO<a8B)*5a}(1G`,>^vni_&vwTmfB' );
define( 'SECURE_AUTH_SALT', 'WyD)GEe&]JLtyLOGK]0]HZ:f n0:oTB>:}DwqQE+6H/v[^e_3Z zP4/u0RtudwDa' );
define( 'LOGGED_IN_SALT',   'vME+A$7A_A@[RJlX<q2KpQY-+tkA8}iZLzB2b#ex5U&1Wp<T8IN)<TJ`?Ww,O+=5' );
define( 'NONCE_SALT',       'jbb,?8*+AE2<@uV,*v1u=HAZ},.ts.]Gw{gzB9>[{ud~gA)]kVpwvFjG]q{? M6&' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
